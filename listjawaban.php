<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>List Jawaban</title>
  </head>

  <body>
    <center>
      <h2>Masukkan Otentikasi Data</h2>
      <form action="#" method="POST">
        <table>
          <tr>
            <td>Username</td>
            <td><input type="text" name="username" placeholder="masukkan username" id="username" /></td>
          </tr>
          <tr>
            <td>Password</td>
            <td><input type="password" name="password" placeholder="masukkan password" id="password" /></td>
          </tr>
          <tr>
            <td>Kode Ujian</td>
            <td><input type="number" name="kode_ujian" placeholder="masukkan kode ujian" id="kode_ujian" /></td>
          </tr>
          <tr>
            <td colspan="3" align="center"><input type="submit" value="submit"></td>
          </tr>
        </table>
      </form>
      <br>
      <pre id="result">
      </pre>
    </center>
  </body>
  
<?php
  if($_POST){
    $username = $_POST['username'];
    $password = $_POST['password'];
    $kode_ujian = $_POST['kode_ujian'];
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,"https://onclass.ffst.my.id/index.php/api/list_jawaban");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,"username=$username&password=$password&kode_ujian=$kode_ujian");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec ($ch);
    curl_close ($ch);
    
    $decoded = json_decode($result, true);
    echo "<pre>".$result."</pre>";
  }
?>
</html>